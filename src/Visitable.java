
public interface Visitable {
	public void accept(Visitor visitory);
	public String giveReport();
}
