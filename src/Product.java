import javax.swing.text.GapContent;

public class Product implements Visitable {
	private String name;
	private double price;

	public Product(String name, double price) {
		this.name = name;
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	@Override
	public void accept(Visitor visitory) {
		System.out.println(giveReport());
		visitory.visit(this);
	}

	@Override
	public String giveReport() {
		return toString();
	}

	public String toString() {
		StringBuilder build = new StringBuilder();
		build.append("\t\t");
		build.append("Product [name=");
		build.append(name);
		build.append(", price=");
		build.append(price);
		build.append("]");

		return build.toString();
	}

}
