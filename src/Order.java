import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Order implements Visitable {
	private String number;
	private double orderTotalPrice;
	private Date orderDate;
	private List<Product> products;

	SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	DecimalFormat dec = new DecimalFormat("###.##");

	// FOR TEST ------------------------
	public Order(String number) {
		this.number = number;
		this.products = new ArrayList<>();
		this.orderDate = new Date();
	}
	// ----------------------------------

	public String getNumber() {
		return number;
	}

	public double getOrderTotalPrice() {
		return orderTotalPrice;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void addProduct(Product product) {
		products.add(product);
		orderTotalPrice += product.getPrice();
	}

	@Override
	public void accept(Visitor visitor) {
		System.out.println(giveReport());
		for (Product product : products) {
			product.accept(visitor);
		}
		visitor.visit(this);
	}

	@Override
	public String giveReport() {
		return toString();
	}

	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("\n\tOrder [number=");
		builder.append(number);
		builder.append("; orderTotalPrice=");
		builder.append(dec.format(orderTotalPrice));
		builder.append("; orderDate=");
		builder.append(dt.format(orderDate));
		builder.append("; products:");
		
		return builder.toString();
	}

}
