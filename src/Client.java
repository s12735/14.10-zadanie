import java.util.ArrayList;
import java.util.List;

import javax.swing.text.GapContent;

import org.omg.Messaging.SyncScopeHelper;

public class Client implements Visitable {
	private String number;
	private List<Order> orders;

	public Client(String number) {
		this.orders = new ArrayList<>();
		this.number = number;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public void addOrder(Order order) {
		orders.add(order);
	}

	@Override
	public void accept(Visitor visitor) {
		System.out.println(giveReport());
		for (Order order : orders) {
			order.accept(visitor);
		}
		visitor.visit(this);
	}

	@Override
	public String giveReport() {
		return toString();
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("\nClient [number=");
		builder.append(number);
		builder.append(", orders=");
		
		return builder.toString();
	}

}
