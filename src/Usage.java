public class Usage {

	public static void main(String[] args) {

		Product p1 = new Product("p1", 21);
		Product p2 = new Product("p2", 324);

		Order ord = new Order("ord");
		ord.addProduct(p1);
		ord.addProduct(p2);

		Order or = new Order("r=or");
		or.addProduct(p1);

		Client client = new Client("ada");
		client.addOrder(ord);
		client.addOrder(or);

		Client client2 = new Client("bob");
		client2.addOrder(ord);

		ClientGroup cligrup = new ClientGroup("cll");

		cligrup.addClient(client);
		cligrup.addClient(client2);

		ReportMaker rm = new ReportMaker();

		// cligrup.accept(rm);
		rm.createRaport(p1); // to samo co wy�ej, w nawiasie czego raport ma zrobi�
								 // np konkretna grupa klient�w, konkretny klient lub
								 // konkretne zam�wienie

	}
}
