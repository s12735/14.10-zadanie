import java.util.ArrayList;
import java.util.List;

public class ClientGroup implements Visitable {
	private String memberName;
	private List<Client> clients;

	public ClientGroup(String memberName) {
		this.memberName = memberName;
		clients = new ArrayList<>();
	}

	public void addClient(Client client) {
		clients.add(client);
	}

	public String toString() {
		return "ClientGroup [memberName=" + memberName + ", clients=\n" + clients + "]\n";
	}

	@Override
	public void accept(Visitor visitor) {

		for (Client c : clients) {
			c.accept(visitor);
		}
		visitor.visit(this);
	}

	@Override
	public String giveReport() {
		return "ClientGroup [memberName=" + memberName + ", clients=]\n";
	}

}
