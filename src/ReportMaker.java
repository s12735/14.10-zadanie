
public class ReportMaker implements Visitor {
	private int numberOfClients = 0;
	private int numberOfOrder;
	private int numberOfProducts;

	public int getNumberOfClients() {
		return numberOfClients;
	}

	public int getNumberOfOrder() {
		return numberOfOrder;
	}

	public int getNumberOfProducts() {
		return numberOfProducts;
	}

	public void createRaport(Visitable visitable){
		visitable.accept(this);
		System.out.println("Client: " + getNumberOfClients());
		System.out.println("Order: " + getNumberOfOrder());
		System.out.println("Product: " + getNumberOfProducts());
	}

	@Override
	public void visit(Visitable visitable) {

		if (visitable instanceof Client) {
			numberOfClients++;
		}
		if (visitable instanceof Order) {
			numberOfOrder++;
		}
		if (visitable instanceof Product) {
			numberOfProducts++;
		}

	}

}
